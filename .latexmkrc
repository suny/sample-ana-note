$pdflatex = 'pdflatex -interaction=nonstopmode %O %S';

# Add LHCb LaTeX templates to PATH
$ENV{TEXINPUTS} = ".:./include:./lhcb-templates/LHCb-latex-template/latest/latex:";
$ENV{BIBINPUTS} = ".:./bib:./lhcb-templates/LHCb-latex-template/latest/latex:";
$ENV{BSTINPUTS} = ".:./bib:./lhcb-templates/LHCb-latex-template/latest/latex:";

# Clean additional tmp files
$clean_ext = "bbl";

# Automatically convert eps to pdf
@cus_dep_list = (@cus_dep_list, "eps pdf 0 eps2pdf");
sub eps2pdf {
   system("epstopdf $_[0].eps"); }
