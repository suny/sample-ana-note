# sample-ana-note

Sample ANA note with gitlab CI to build a new pdf file on each commit.

The latest pdf files can be viewed at [1].


## Compilation

### Compile locally, without `docker`

Clone the project with:
```
git clone --recurse-submodules --remote-submodules
```

Make sure to have a working LaTeX installation. Then compile:
```
latexmk -pdf
```

To cleanup tmp files generated during the compilation:
```
latexmk -c
```

### Compile locally, with `docker`

Install `docker` on your machine first, then:
```
docker run --rm -it -v $(pwd)/data yipengsun:sample-ana-note
```

This will give you a prompt, which is a shell running inside docker. Now type:
```
latexmk -pdf
```


## Tips and tricks

### Automatically convert `.eps` figures to `.pdf`

Add the following lines to `.latexmkrc`:

```perl
@cus_dep_list = (@cus_dep_list, "eps pdf 0 eps2pdf");
sub eps2pdf {
   system("epstopdf $_[0].eps"); }
```

For more info, see [2].


[1]: https://gitlab.cern.ch/suny/sample-ana-note/-/jobs/artifacts/master/browse?job=build
[2]: https://mg.readthedocs.io/latexmk.html
